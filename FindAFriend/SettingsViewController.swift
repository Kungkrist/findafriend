//
//  SettingsViewController.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-22.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit
import Foundation

class SettingsViewController: UIViewController {

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var textFieldForSlider: UITextField!
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    let settings = Settings.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        self.textFieldForSlider.text = Int(self.settings.radiusInMeters).description
        self.slider.value = Float(self.settings.radiusInMeters)
        
        if settings.visible {
            self.segmentController.selectedSegmentIndex = 0
        }else {
            self.segmentController.selectedSegmentIndex = 1
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveClick(sender: UIBarButtonItem) {
        
        // Display a popup if the radius is to big.
        if Double(self.textFieldForSlider.text!) > 100000 {
            let alertController = UIAlertController(title: "Message", message:
                "You cannot create a radius that big.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
            return
        }
        
        settings.radiusInMeters = Double(self.textFieldForSlider.text!)!
        
        if self.segmentController.selectedSegmentIndex == 0 {
            settings.visible = true
        } else {
            settings.visible = false
        }
        
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func sliderDrag(sender: UISlider) {
        self.textFieldForSlider.text = Int(self.slider.value).description
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
