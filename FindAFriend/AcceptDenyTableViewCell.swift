//
//  AcceptDenyTableViewCell.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-14.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit

protocol AcceptDenyProtocol {
    func didDenyRequest(account : String)
    func didAcceptRequest(account : String)
}

class AcceptDenyTableViewCell: UITableViewCell, JSONProtocol{

    @IBOutlet weak var username: UILabel!
    var user : Account?
    var delegate : AcceptDenyProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func denyClick(sender: UIButton) {
        if let account = user {
            if let u = self.username {
                
                if account.deleteRecievedFriendRequest(u.text!) {
                    JSONHelper.jsonGetData(self, url: "http://78.72.213.61:1338/accounts?username=" + u.text!, identifier: 10)
                }
            }
        }
    }

    @IBAction func acceptClick(sender: UIButton) {
        if let account = user {
            if let u = self.username {
                
                if account.deleteRecievedFriendRequest(u.text!) {
                    JSONHelper.jsonGetData(self, url: "http://78.72.213.61:1338/accounts?username=" + u.text!, identifier: 1)
                }
            }
        }
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func didReceiveResponse(result: AnyObject?, _ identifier: Int) {
        
        // Find the specified user and add him as a friend
        if identifier == 1 {
            changeSpecifiedUserSettings(result as! NSArray, makeFriend: true, identifier: 2)
            
        }
        
        // Add the specified "Friend" to the user's friend-list.
        else if identifier == 2 {
            self.user!.addNewFriend(self.username.text!)
            JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + self.user!.id.description, method: "PUT", data: self.user!.toDictionary(), identifier: 100)
        
        }
        
        // Find the specified user and deny him as a friend.
        else if identifier == 10 {
            changeSpecifiedUserSettings(result as! NSArray, makeFriend: false, identifier: 100)
        }
            
        else if identifier == 100 {
            self.delegate!.didAcceptRequest(self.username.text!)
        }
    }
    
    func didFailToReceiveResponse(result: AnyObject?, _ identifier: Int) {
        
    }
    
    func changeSpecifiedUserSettings(result: NSArray, makeFriend: Bool, identifier: Int) {
        
        let data : NSDictionary = (result[0]) as! NSDictionary
        let arr : [String] = data["sent-friend-requests"] as! [String]
        let data2 = data
        
        if arr.count > 0 {
            for (index, value) in arr.enumerate() {
                if value == self.user?.username {
                    data2["sent-friend-requests"]?.removeObjectAtIndex(index)
                    
                    if makeFriend {
                        if let username = self.user {
                            
                            // Add the user to the specified "friend"'s friend-list
                            var arr : [String] = data["friends"] as! [String]
                            arr.append(username.username)
                            
                            data2.setValue(arr, forKey: "friends")
                        }
                    }
                    break
                }
            }
        } else {
            // Do nothing for noq
        }
        let id : Int = data2["id"] as! Int
        let idAsString : String = id.description
        
        // Remove the id value drom data (backend doesn't want id when doing a PUT request)
        data2.setValue("asd", forKey: "id")
        print("result: \(data2)")
        JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + idAsString, method: "PUT", data: data2, identifier: identifier)
    }

}
