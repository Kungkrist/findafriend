//
//  FriendsTableViewController.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-14.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit

protocol UpdateUserProtocol {
    func didUpdateUser(user : Account)
}

class FriendsTableViewController: UITableViewController, JSONProtocol, UpdateUserProtocol {

    var user : Account?
    var delegate : UpdateUserProtocol?
    var offlineFriends : [Account] = []
    var onlineFriends : [Account] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set background color to dark gray
        self.tableView.backgroundColor = UIColor(red: 60.0/255, green: 64.0/255, blue: 65.0/255, alpha: 1.0)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewDidAppear(animated: Bool) {
        self.onlineFriends = []
        self.offlineFriends = []
        
        if let account = user {
            let friends = account.friends
            
            if friends.count > 0 {
                let arr : [String] = friends
                for x in arr {
                    JSONHelper.jsonGetData(self, url: "http://78.72.213.61:1338/accounts?username=" + x, identifier: 1)
                }
            } else {
                //print("No friends found!")
            }
        } else {
            //print("No logged in account found!")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.onlineFriends.count
        }
        
        return self.offlineFriends.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("onlineCell", forIndexPath: indexPath) as! FriendTableViewCell
            
            cell.username.text = self.onlineFriends[indexPath.row].username
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("offlineCell", forIndexPath: indexPath) as! FriendTableViewCell
            
            cell.username.text = self.offlineFriends[indexPath.row].username
            
            return cell
        }
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            if indexPath.section == 0 {
                if self.user!.deleteFriend(self.onlineFriends[indexPath.row].username) {
                    JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + user!.id.description, method: "PUT", data: user!.toDictionaryWithoutId(), identifier: -1)
                    
                    let friend = self.onlineFriends[indexPath.row]
                    if friend.deleteFriend(user!.username) {
                        JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/"
                            + friend.id.description, method: "PUT", data: friend.toDictionaryWithoutId(), identifier: -1)
                    }
                }
                
                self.onlineFriends.removeAtIndex(indexPath.row)
            }else {
                if self.user!.deleteFriend(self.offlineFriends[indexPath.row].username) {
                    JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + user!.id.description, method: "PUT", data: user!.toDictionaryWithoutId(), identifier: -1)
                    
                    let friend = self.offlineFriends[indexPath.row]
                    if friend.deleteFriend(user!.username) {
                        JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/"
                            + friend.id.description, method: "PUT", data: friend.toDictionaryWithoutId(), identifier: -1)
                    }
                }
                
                self.offlineFriends.removeAtIndex(indexPath.row)
            }
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "ToAddFirendSegue" {
            let destination : AddFriendTableViewController = segue.destinationViewController as! AddFriendTableViewController
            
            destination.user = self.user
            destination.delegate = self
        }
    }
    
    
    func didReceiveResponse(result: AnyObject?, _ identifier: Int) {
        
        // Get request of a specified friend
        if(identifier == 1) {
           // let data : NSDictionary = result![0]
            
            if let data : NSDictionary = result?.firstObject as? NSDictionary{
                let isActive : String = data["is-active"] as! String
                if isActive == "yes" {
                    onlineFriends.append(Account.init(fullData: data))
                    self.performSelectorOnMainThread(#selector(reloadTableView), withObject: "", waitUntilDone: false)
                }else {
                    offlineFriends.append(Account.init(fullData: data))
                    self.performSelectorOnMainThread(#selector(reloadTableView), withObject: "", waitUntilDone: false)
                }
            }            
        }
    }
    
    func didFailToReceiveResponse(result: AnyObject?, _ identifier: Int) {
        displayPopup("ERROR", body: "Something went wrong :(", button: "OK")
    }
    
    func didUpdateUser(user: Account) {
        self.delegate?.didUpdateUser(user)
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
    // Display a popup
    func displayPopup(title : String, body : String, button : String) {
        let alertController = UIAlertController(title: title, message:
            body, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.Default,handler: nil))
        self.performSelectorOnMainThread(#selector(presentPopup), withObject: alertController, waitUntilDone: false)
    }
    
    func presentPopup(alertController : UIAlertController) {
        self.presentViewController(alertController, animated: true, completion: nil)
    }

}
