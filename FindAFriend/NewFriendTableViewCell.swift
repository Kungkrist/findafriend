//
//  NewFriendTableViewCell.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-15.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit

protocol NewFriendProtocol {
    func didAddFriend(account : String)
}

class NewFriendTableViewCell: UITableViewCell, JSONProtocol {

    @IBOutlet weak var textField: UITextField!
    var user : Account?
    var delegate : NewFriendProtocol?
    var delegateForPopup : UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    @IBAction func addClick(sender: UIButton) {
        JSONHelper.jsonGetData(self, url: "http://78.72.213.61:1338/accounts?username=" + self.textField.text!.lowercaseString, identifier: 1)
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func didReceiveResponse(result: AnyObject?, _ identifier: Int) {
        if identifier == 1 {
            let arr : NSArray = result as! NSArray
            
            // if the account doesn't exist, exit this function.
            if arr.count == 0{
                displayPopup("Message", body: "Account does not exist!", button: "OK")
                return
            }
            
            let data : NSDictionary = arr[0] as! NSDictionary
            
            if let account = self.user {
                var accountAlreadyExists : Bool = false
                
                for d in data["recieved-friend-requests"] as! [String] {
                    if d == account.username {
                        accountAlreadyExists = true
                        displayPopup("Message", body: "Account already exsists!", button: "OK")
                        break
                    }
                }
                
                if !accountAlreadyExists {
                    var arr : [String] = data["recieved-friend-requests"] as! [String]
                    arr.append(account.username)
                    
                    let data2 = data
                    data2.setValue(arr, forKey: "recieved-friend-requests")
                    let idAsString = data2["id"]!.description
                    
                    JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + idAsString, method: "PUT", data: data2, identifier: 2)
                } else {
                    account.addNewSendRequest(self.textField.text!)
                    JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + account.id.description, method: "PUT", data: account.toDictionary(), identifier: 100)
                }
            }
        }
        
        else if identifier == 2 {
        
            self.user!.addNewSendRequest(self.textField.text!)
            JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + self.user!.id.description, method: "PUT", data: self.user!.toDictionary(), identifier: 100)
        }
        
        else if identifier == 100 {
            self.delegate?.didAddFriend(self.textField.text!)
        }
    }
    
    func didFailToReceiveResponse(result: AnyObject?, _ identifier: Int) {
        displayPopup("ERROR", body: "Something went wrong :(", button: "OK")
    }
    
    // Display a popup
    func displayPopup(title : String, body : String, button : String) {
        let alertController = UIAlertController(title: title, message:
            body, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.Default,handler: nil))
        self.performSelectorOnMainThread(#selector(presentPopup), withObject: alertController, waitUntilDone: false)
    }
    
    func presentPopup(alertController : UIAlertController) {
        self.delegateForPopup?.presentViewController(alertController, animated: true, completion: nil)
    }

}
