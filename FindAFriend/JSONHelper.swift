//
//  JSONHelper.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-09.
//  Copyright © 2016 DEA. All rights reserved.
//


import UIKit

protocol JSONProtocol {
    func didReceiveResponse(result: AnyObject?, _ identifier: Int)
    func didFailToReceiveResponse(result: AnyObject?, _ identifier: Int)
}

class JSONHelper {
    
    static func jsonSendData(sender:JSONProtocol, url:String, method:String, data:NSDictionary!, identifier: Int) {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(data!, options: [])
        
        
        let task = session.dataTaskWithRequest(request) { data, response, error in
            guard data != nil else {
                print("no data found: \(error)")
                return
            }
            
            do {
                if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                    let success = json["success"] as? Int
                    sender.didReceiveResponse(success, identifier)
                } else {
                    let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    sender.didFailToReceiveResponse(jsonStr, identifier)
                }
            } catch let parseError {
                print(parseError)
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                sender.didFailToReceiveResponse(jsonStr, identifier)
            }
        }
        
        task.resume()
    }
    
    static func jsonGetData(sender: JSONProtocol, url: String, identifier: Int) {
        let URL = NSURL(string: url)!
        
        let session = NSURLSession.sharedSession()
        
        session.dataTaskWithURL(URL, completionHandler: { ( data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            guard let realResponse = response as? NSHTTPURLResponse where
                realResponse.statusCode == 200 else {
                    sender.didFailToReceiveResponse("Not a 200 response", identifier)
                    return
            }
            
            do {
                if (NSString(data:data!, encoding: NSUTF8StringEncoding) != nil) {
                    
                    let result = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                    
                    sender.didReceiveResponse(result, identifier)
                }
            } catch {
                sender.didFailToReceiveResponse("Error!", identifier)
            }
        }).resume()
        
    }
    
    
}
