//
//  Account.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-11.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit

class Account {
    private let _username : String
    private var _firstName : String = ""
    private var _lastName : String = ""
    private var _currentLatitude : Double = 0.0
    private var _currentLongitude : Double = 0.0
    private var _isActive : String = "no";
    private var _id : Int = 0
    private var _date : String = ""
    private var _time : String = ""
    private var _sentFriendRequests : [String] = []
    private var _recievedFriendRequests : [String] = []
    private var _friends : [String] = []
    
    init(username:String, id:Int) {
        self._username = username
        self._id = id
    }
    
    init(fullData: NSDictionary) {
        self._username = fullData["username"] as! String
        self._id = fullData["id"] as! Int
        self.firstName = fullData["first-name"] as! String
        self.lastName = fullData["last-name"] as! String
        self.date = fullData["date"] as! String
        self.time = fullData["time"] as! String
        self.isActive = fullData["is-active"] as! String
        self.friends = fullData["friends"] as! [String]
        self.recievedFriendRequests = fullData["recieved-friend-requests"] as! [String]
        self.sentFriendRequests = fullData["sent-friend-requests"] as! [String]
        
        if ((fullData["current-lat"] as? String) == nil){
            self.currentLatitude = fullData["current-lat"] as! Double
        }
        if ((fullData["current-long"] as? String) == nil){
            self.currentLongitude = fullData["current-long"] as! Double
        }
        
    }
    
    var username : String {
        get {
            return self._username
        }
    }
    
    
    var id : Int {
        get {
            return self._id
        }
    }
    
    var firstName : String {
        get {
           return self._firstName
        }
        
        set (value){
            self._firstName = value
        }
    }
    
    var lastName : String {
        get {
            return self._lastName
        }
        
        set (value){
            self._lastName = value
        }
    }
    
    var currentLatitude : Double {
        get {
            return self._currentLatitude
        }
        
        set (value){
            self._currentLatitude = value
        }
    }
    
    var currentLongitude : Double {
        get {
            return self._currentLongitude
        }
        
        set (value){
            self._currentLongitude = value
        }
    }
    
    var isActive : String {
        get {
            return self._isActive
        }
        
        set (value){
            self._isActive = value
        }
    }
    
    var date : String {
        get {
            return self._date
        }
        
        set (value){
            self._date = value
        }
    }
    
    var time : String {
        get {
            return self._time
        }
        
        set (value){
            self._time = value
        }
    }
    
    var sentFriendRequests : [String] {
        get {
            return self._sentFriendRequests
        }
        
        set (value){
            self._sentFriendRequests = value
        }
    }
    
    var recievedFriendRequests : [String] {
        get {
            return self._recievedFriendRequests
        }
        
        set (value){
            self._recievedFriendRequests = value
        }
    }
    
    var friends : [String] {
        get {
            return self._friends
        }
        
        set (value){
            self._friends = value
        }
    }
    
    func addNewFriend(value : String) {
        self.friends.append(value)
    }
    
    // Append a new send request
    func addNewSendRequest(value : String) {
        self.sentFriendRequests.append(value)
    }
    
    // Append a new recieved request
    func addNewRecievedRequest(value : String) {
        self.recievedFriendRequests.append(value)
    }
    
    // Delete a send request from list
    func deleteSentFriendRequest(value : String) -> Bool {
        let index = delete(self.sentFriendRequests, value: value)
        self.sentFriendRequests.removeAtIndex(index)
        
        return index >= 0
    }
    
    // Delete a recieved request from list
    func deleteRecievedFriendRequest(value : String) -> Bool {
        let index = delete(self.recievedFriendRequests, value: value)
        if index != -1 {
            self.recievedFriendRequests.removeAtIndex(index)
        }
        return index >= 0
    }
    
    func deleteFriend(value : String) -> Bool {
        let index = delete(self.friends, value: value)
        if index != -1 {
            self.friends.removeAtIndex(index)
        }
        return index >= 0
    }
    
    // Iterating through an array and returns an index for the specified item found in list.
    private func delete(arr : [String], value : String) -> Int {
        for (index, val) in arr.enumerate() {
            if val == value {
                return index
            }
        }
        
        return -1
    }
    
    // This will return this user as an NSDictionary
    func toDictionary() -> NSDictionary {
        let dic : NSDictionary = ["username" : self.username,
                                  "first-name" : self.firstName,
                                  "last-name" : self.lastName,
                                  "id" : self.id,
                                  "current-lat" : self.currentLatitude,
                                  "current-long" : self.currentLongitude,
                                  "date" : self.date,
                                  "time" : self.time,
                                  "is-active" : self.isActive,
                                  "sent-friend-requests" : self.sentFriendRequests,
                                  "recieved-friend-requests" : self.recievedFriendRequests,
                                  "friends": self.friends
                                  ]
        return dic
    }
    
    // This will return this user as an NSDictionary
    func toDictionaryWithoutId() -> NSDictionary {
        let dic : NSDictionary = ["username" : self.username,
                                  "first-name" : self.firstName,
                                  "last-name" : self.lastName,
                                  "current-lat" : self.currentLatitude,
                                  "current-long" : self.currentLongitude,
                                  "date" : self.date,
                                  "time" : self.time,
                                  "is-active" : self.isActive,
                                  "sent-friend-requests" : self.sentFriendRequests,
                                  "recieved-friend-requests" : self.recievedFriendRequests,
                                  "friends": self.friends
        ]
        return dic
    }
    
    // returns isActive as a boolean
    func isActiveAsBool() -> Bool {
        if self.isActive == "yes" {
            return true
        }
        
        return false
    }
    
    

}
