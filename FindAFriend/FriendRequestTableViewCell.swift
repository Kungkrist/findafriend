//
//  FriendRequestTableViewCell.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-14.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit

class FriendRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var username: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
