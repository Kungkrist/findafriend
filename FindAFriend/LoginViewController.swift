//
//  LoginViewController.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-09.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit

// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension String {
    func containsSpace() -> Bool {
        if self.componentsSeparatedByString(" ").count > 1{
            return true
        }
        
        return false
    }
}

class LoginViewController: UIViewController, JSONProtocol {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var button: UIButton!
    
    var user : Account?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        setViewToLogin()
        self.navigationController?.navigationBar.barTintColor = UIColor.orangeColor()
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    enum RequestIdentifiers {
        case createAccount
        case login
        case creatingAccount
        case loggingIn
        case setAccountToActive
        case logout
        
        var getValue : Int {
            get {
                switch(self) {
                case .createAccount:
                    return 1
                case .login:
                    return 2
                case .creatingAccount:
                    return 10
                case .loggingIn:
                    return 11
                case .setAccountToActive:
                    return 12
                case .logout:
                    return 13
                }
            }
        }
    }
    
    @IBAction func segmentClick(sender: UISegmentedControl) {
        if(sender.selectedSegmentIndex == 0) {
            setViewToLogin()
        }else {
            setViewToCreateAccount()
        }
    }
    
    func setViewToLogin() {
        self.button.setTitle("Login", forState: .Normal)
        self.titleLabel.text = "Login"
        self.lastNameField.hidden = true;
        self.firstNameField.hidden = true;
    }
    
    func setViewToCreateAccount() {
        self.button.setTitle("Create account", forState: .Normal)
        self.titleLabel.text = "Create account"
        self.lastNameField.hidden = false;
        self.firstNameField.hidden = false;
    }
    
    @IBAction func doneClick(sender: UIButton) {
        let username = self.usernameField.text
        
        // If the user is trying to create a new account.
        if(self.segmentController.selectedSegmentIndex == 1) {
            let firstName = self.firstNameField.text
            let lastName = self.lastNameField.text
            
            if (username?.containsSpace())! || (firstName?.containsSpace())! || (lastName?.containsSpace())! {
                displayPopup("Message", body: "Text-fields cannot contain any spaces", button: "OK")
                return
            }
            
            if (username != nil && username != "") && (firstName != nil && firstName != "") && (lastName != nil && lastName != "") {
                
                // Try to create an account
                JSONHelper.jsonGetData(self, url: "http://78.72.213.61:1338/accounts/", identifier: RequestIdentifiers.createAccount.getValue)
                
            }
        }
        
        // If the user is trying to login
        else {
            if username != nil && username != "" {
                
                // Try to login.
                JSONHelper.jsonGetData(self, url: "http://78.72.213.61:1338/accounts/", identifier: RequestIdentifiers.login.getValue)
            }
        }
    }
    
    func didReceiveResponse(result: AnyObject?, _ identifier: Int) {
        
        // When the user is trying to create a new account
        if(identifier == RequestIdentifiers.createAccount.getValue) {
            
            let parsedResult = (result as! NSArray) as Array
            let username = self.usernameField.text!.lowercaseString
            let firstName = self.firstNameField.text!
            let lastName = self.lastNameField.text!
            
            // true if name is found on server
            var foundUsername : Bool = false;
            
            // look if the name is found on the server
            for val in parsedResult {
                let x = val as! NSDictionary
                let existingUsername : String = x["username"] as! String
                
                if(existingUsername == username) {
                    foundUsername = true
                    break;
                }
            }
            
            // create a new account on the server
            if !foundUsername {
                let data : NSDictionary = [ "username": username,
                                            "first-name": firstName,
                                            "last-name": lastName,
                                            "sent-friend-requests" : [],
                                            "recieved-friend-requests": [],
                                            "friends": [],
                                            "current-lat": "",
                                            "current-long": "",
                                            "date": "",
                                            "time": "",
                                            "is-active": "no"]
                
                JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts", method: "POST", data: data, identifier: RequestIdentifiers.creatingAccount.getValue)
            } else {
                displayPopup("Message", body: "Account already exists!", button: "OK")
                self.performSelectorOnMainThread(#selector(updateLabel), withObject: "", waitUntilDone: false)
            }
        }
            
        // When the user is trying to login to an existing account
        else if(identifier == RequestIdentifiers.login.getValue) {
            let parsedResult = (result as! NSArray) as Array
            let username = self.usernameField.text!.lowercaseString
            
            // true if name is found on server
            var foundUsername : Bool = false;
            
            // true if the account is not currently active
            var isNotActive : Bool = false;
            
            // represents a specified user
            var user : NSDictionary = [:];
            
            // look if the name is found on the server
            for val in parsedResult {
                let x = val as! NSDictionary
                let existingUsername : String = x["username"] as! String
                
                if(existingUsername == username) {
                    user = x
                    foundUsername = true
                    
                    if user["is-active"] as! String == "no" {
                        isNotActive = true
                    }
                    
                    break;
                }
            }
            
            // If the account exsist & is not already active.
            if foundUsername && isNotActive{
                user.setValue("yes", forKey: "is-active")
                
                self.user = Account.init(fullData: user)
                
                if let account = self.user {
                    // Login to the specified account.
                    JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + account.id.description, method: "PUT", data: account.toDictionary(), identifier: RequestIdentifiers.setAccountToActive.getValue)
                }
                
                
            } else {
                if(!foundUsername) {
                    displayPopup("Message", body: "Account does not exist!", button: "OK")
                    
                } else {
                    displayPopup("Message", body: "Account is already active!", button: "OK")
                    
                }
            }
        }
        
        // When the user has done the put-request, start a segue.
        else if(identifier == RequestIdentifiers.setAccountToActive.getValue) {
            
            self.performSelectorOnMainThread(#selector(startSegue), withObject: "", waitUntilDone: true)
        }
        
        // When the user has created a new account.
        else if (identifier == RequestIdentifiers.creatingAccount.getValue) {
            self.performSelectorOnMainThread(#selector(changeToLoginView), withObject: "", waitUntilDone: false)
        }
        
    }
    
    // Start a segue to the MainViewController
    func startSegue() {
        self.performSegueWithIdentifier("ToMainSegue", sender: nil)
    }
    
    // Change the segment controller to "login"
    func changeToLoginView() {
        setViewToLogin()
        self.segmentController.selectedSegmentIndex = 0
    }
    
    // Set the username to empty
    func updateLabel() {
        self.usernameField.text = "";
    }
    
    // If the http-request failed.
    func didFailToReceiveResponse(result: AnyObject?, _ identifier: Int) {
        displayPopup("ERROR!", body: "Something went wrong :(", button: "OK")
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "ToMainSegue" {
            let destination : MapViewController = segue.destinationViewController as! MapViewController
            destination.user = self.user
        }
    }
    
    // Display a popup
    func displayPopup(title : String, body : String, button : String) {
        let alertController = UIAlertController(title: title, message:
            body, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.Default,handler: nil))
        self.performSelectorOnMainThread(#selector(presentPopup), withObject: alertController, waitUntilDone: false)
    }
    
    func presentPopup(alertController : UIAlertController) {
        self.presentViewController(alertController, animated: true, completion: nil)
    }

}
