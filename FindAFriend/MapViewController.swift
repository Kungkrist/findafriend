//
//  MapViewController.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-11.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit



class MapViewController: UIViewController, JSONProtocol, MKMapViewDelegate, UITabBarDelegate, CLLocationManagerDelegate, UpdateUserProtocol, LogoutProtocol, FoundNewFriendInCirclePrococol{
    
    var user : Account?
    let locationManager : CLLocationManager = CLLocationManager()
    var lastCircle : MKCircle?
    var timer : NSTimer?
    var sendRequestIsReady : Bool = true
    let settings = Settings.instance
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var mapView: MKMapView!
    
    func didLogout() {
        if let account = user {
            account.isActive = "no"
            let data : NSDictionary = account.toDictionary()
            
            // Login to the specified account.
            JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + account.id.description, method: "PUT", data: data, identifier: -1)
            
            locationManager.stopUpdatingLocation()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
            
            
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            locationManager.allowsBackgroundLocationUpdates = true
        }
        
        
        // Set up the tab bar
        self.tabBar.delegate = self
        self.tabBar.tintColor = UIColor.blackColor()
        let items : [UITabBarItem] = self.tabBar.items!
        for (index, item) in items.enumerate() {
            item.tag = index
            
            // Change the text color
            item.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
            
            // Change the image color
            item.image = item.image?.imageWithRenderingMode(.AlwaysOriginal)
        }
        
        mapView.showsUserLocation = true
        mapView.delegate = self
        
        timer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
        timer!.fire()
        
    }
    
    @IBAction func updateClick(sender: UIBarButtonItem) {
        updateMap()
    }
    
    // Set upp the zoom level of the mapview
    func updateMap() {
        if self.locationManager.location != nil {
        var latDelta : Double?
        var longDelta : Double?
        
        if let circle = RadiusLogic.circle {
            latDelta = circle.radius * 0.00003
            longDelta = circle.radius * 0.00003
        }else {
            latDelta = 0.01
            longDelta = 0.01
        }
        
        let tajSpan = MKCoordinateSpan(latitudeDelta: latDelta!, longitudeDelta: longDelta!)
        
        let tajRegion = MKCoordinateRegion(center: self.locationManager.location!.coordinate, span: tajSpan)
        self.mapView.setRegion(tajRegion, animated: true)
        
        }
        
        tick()
    }
    
    override func viewDidAppear(animated: Bool) {
        updateMap()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if sendRequestIsReady {
            let locValue:CLLocationCoordinate2D = manager.location!.coordinate
            
            if self.user != nil {
                let center = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
                
                let circle : MKCircle = MKCircle.init(centerCoordinate: center, radius: settings.radiusInMeters)
                RadiusLogic.circle = circle
                
                // If the user is visible to other people
                if settings.visible {
                    user!.currentLatitude = locValue.latitude
                    user!.currentLongitude = locValue.longitude
                } else {
                    user!.currentLatitude = 0.0
                    user!.currentLongitude = 0.0
                }
                
                JSONHelper.jsonSendData(self, url: "http://78.72.213.61:1338/accounts/" + user!.id.description, method: "PUT", data: user!.toDictionary(), identifier: -1)
                
                for a in user!.friends {
                    JSONHelper.jsonGetData(self, url: "http://78.72.213.61:1338/accounts?username=" + a, identifier: 1)
                }
            }
            
            sendRequestIsReady = false
        }
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        if item.tag == 0 {
            performSegueWithIdentifier("toFriendsSegue", sender: nil)
        } else if item.tag == 1 {
            performSegueWithIdentifier("toSettingsSegue", sender: nil)
        }
    }
    
    func tick() {
        if let friends = RadiusLogic.friendsInsideRadius {
            if friends.count > 0 {
                
                // Remove old pins
                if self.mapView.annotations.count > 0 {
                    self.mapView.removeAnnotations(self.mapView.annotations)
                }
                
                // Add pins of friends inside the circle.
                for a in friends {
                    let location = CLLocationCoordinate2DMake(a.currentLatitude, a.currentLongitude)
                    
                    // Drop a pin
                    let dropPin = MKPointAnnotation()
                    dropPin.coordinate = location
                    dropPin.title = a.firstName + " " + a.lastName
                    dropPin.subtitle = "(" + a.username + ")"
                    self.mapView.addAnnotation(dropPin)
                    
                }
            }
        }
        
        
        // Remove the old circle (radius)
        if let circle = self.lastCircle {
            mapView.removeOverlay(circle)
        }
        
        if let c = RadiusLogic.circle {
            self.lastCircle = c
            self.mapView.addOverlay(c)
        }
        
        sendRequestIsReady = true
    }
    
    func didReceiveResponse(result: AnyObject?, _ identifier: Int) {
        if identifier == 1 {
            let arr : NSArray = result as! NSArray
            let dic : NSDictionary = arr[0] as! NSDictionary
            
            let currentAccount : Account = Account.init(fullData: dic)
            if currentAccount.isActiveAsBool() {
                RadiusLogic.isFriendsInsideRadius(self, friend: currentAccount)
            }
        }
    }
    
    func didFailToReceiveResponse(result: AnyObject?, _ identifier: Int) {
        displayPopup("ERROR", body: "Something went wrong :(", button: "OK")
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.redColor()
            circle.fillColor = UIColor(red: 0, green: 255, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        }
        
        return MKOverlayRenderer.init()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       
        if segue.identifier == "toFriendsSegue" {
            let destination : FriendsTableViewController = segue.destinationViewController as! FriendsTableViewController
            
            destination.user = self.user
            destination.delegate = self
        }
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        if (!(parent?.isEqual(self.parentViewController) ?? false)) {
            self.timer!.invalidate()
            mapView.removeAnnotations(mapView.annotations)
            RadiusLogic.friendsInsideRadius = []
            didLogout()
        }
    }
    
    func didFindNewFriendInCircle(friend: Account) {
        let notification : UILocalNotification = UILocalNotification()
        notification
        notification.alertTitle = "New friend inside your radius!"
        notification.alertBody = friend.firstName + " " + friend.lastName + " (" + friend.username + ")"
            + "\n" + "Has entered your specified radius."
        
        notification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    
    func didUpdateUser(user: Account) {
        self.user = user
    }
    
    // Display a popup
    func displayPopup(title : String, body : String, button : String) {
        let alertController = UIAlertController(title: title, message:
            body, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.Default,handler: nil))
        self.performSelectorOnMainThread(#selector(presentPopup), withObject: alertController, waitUntilDone: false)
    }
    
    func presentPopup(alertController : UIAlertController) {
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}
