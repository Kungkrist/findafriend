//
//  Settings.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-23.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit

class Settings: NSObject {
    
    private static var _instance : Settings?
    static var instance : Settings {
        get {
            if _instance == nil {
                _instance = Settings.init()
            }
            
            return _instance!
        }
    }
    
    private var _radiusInMeters : Double?
    var radiusInMeters : Double {
        get {
            if let radius = _radiusInMeters {
                return radius
            } else {
                // 1000 meter is the standard
                return 1000
            }
        }
        
        set (value){
            _radiusInMeters = value
        }
    }
    
    var visible : Bool = true
    
    
    private override init() {
        
    }

}
