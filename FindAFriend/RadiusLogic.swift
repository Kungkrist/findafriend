//
//  RadiusLogic.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-19.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit
import Foundation
import MapKit

protocol FoundNewFriendInCirclePrococol {
    func didFindNewFriendInCircle(friend: Account)
}

class RadiusLogic: NSObject {
    
    static var circle: MKCircle?
    static var friendsInsideRadius: [Account]?
    
    static func isFriendsInsideRadius(sender: FoundNewFriendInCirclePrococol, friend : Account) {
        let location = CLLocation(latitude: friend.currentLatitude, longitude: friend.currentLongitude)
        
        if let c = self.circle {
            let radius = c.radius
            let circleCordinate = c.coordinate
            let circlePosition = CLLocation(latitude: circleCordinate.latitude, longitude: circleCordinate.longitude)

            let meters : CLLocationDistance = circlePosition.distanceFromLocation(location)
            
            // If the friend is inside the circle
            if meters <= radius {
                
                if self.friendsInsideRadius != nil {
                    for (index, f) in self.friendsInsideRadius!.enumerate().reverse() {
                        // If the friend alredy was inside the circle
                        if friend.id == f.id {
                            friendsInsideRadius!.removeAtIndex(index)
                            friendsInsideRadius!.append(friend)
                            return
                        }
                    }
                    
                    // If the friend was not inside the circle
                    friendsInsideRadius!.append(friend)
                    
                    // Send callback to sender
                    sender.didFindNewFriendInCircle(friend)
                    return
                    
                }else {
                    self.friendsInsideRadius = [friend]
                    
                    // Send callback to sender
                    sender.didFindNewFriendInCircle(friend)
                    return
                }
                
            } else {
                if self.friendsInsideRadius != nil {
                    for (index, f) in self.friendsInsideRadius!.enumerate() {
                        if friend.id == f.id {
                            self.friendsInsideRadius!.removeAtIndex(index)
                            return
                        }
                    }
                }
            
            }
        }
        return
    }

}
