//
//  AddFriendTableViewController.swift
//  FindAFriend
//
//  Created by DEA on 2016-04-14.
//  Copyright © 2016 DEA. All rights reserved.
//

import UIKit

class AddFriendTableViewController: UITableViewController, AcceptDenyProtocol, NewFriendProtocol {
    
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    var user : Account?
    var sentRequests : [String] = []
    var recievedRequests : [String] = []
    var delegate : UpdateUserProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set background color to dark gray
        self.tableView.backgroundColor = UIColor(red: 60.0/255, green: 64.0/255, blue: 65.0/255, alpha: 1.0)
        
        if let account = user {
            self.recievedRequests = account.recievedFriendRequests
            self.sentRequests.appendContentsOf(account.sentFriendRequests)
        }
        
        self.tableView.reloadData()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    @IBAction func segmentClick(sender: UISegmentedControl) {
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.segmentController.selectedSegmentIndex == 0 {
            return (self.sentRequests.count + 1)
        }
        return self.recievedRequests.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if self.segmentController.selectedSegmentIndex == 0 {
            if indexPath.row == 0 {
                let cell : NewFriendTableViewCell = tableView.dequeueReusableCellWithIdentifier("AddFriendCell", forIndexPath: indexPath) as! NewFriendTableViewCell
                
                cell.delegate = self
                cell.user = self.user
                cell.delegateForPopup = self
                
                return cell
            } else {
                let cell : FriendRequestTableViewCell = tableView.dequeueReusableCellWithIdentifier("FriendRequestCell", forIndexPath: indexPath) as! FriendRequestTableViewCell
                
                // Configure the cell...
                cell.username.text = self.sentRequests[indexPath.row - 1]
                
                return cell
            }
        } else {
            let cell : AcceptDenyTableViewCell = tableView.dequeueReusableCellWithIdentifier("AcceptDenyFriendCell", forIndexPath: indexPath) as! AcceptDenyTableViewCell
            
            // Configure the cell...
            cell.username.text = self.recievedRequests[indexPath.row]
            cell.user = self.user
            cell.delegate = self
            
            return cell
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // When the user accepted a friend request
    func didAcceptRequest(account: String) {
        for (index, s) in self.recievedRequests.enumerate() {
            if s == account {
                print(s)
                self.recievedRequests.removeAtIndex(index)
                self.performSelectorOnMainThread(#selector(reloadTableView), withObject: "", waitUntilDone: false)
                return
            }
        }
    }
    
    // When the user denyed a friend request
    func didDenyRequest(account: String) {
        for (index, s) in self.recievedRequests.enumerate() {
            if s == account {
                print(s)
                self.recievedRequests.removeAtIndex(index)
                self.performSelectorOnMainThread(#selector(reloadTableView), withObject: "", waitUntilDone: false)
                return
            }
        }
    }
    
    func didAddFriend(account: String) {
        self.sentRequests.append(account)
        self.performSelectorOnMainThread(#selector(reloadTableView), withObject: "", waitUntilDone: false)
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
        self.delegate!.didUpdateUser(self.user!)
    }

}
